import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Game } from '../pages/game/game';
import { Games } from '../pages/games/games';
import { User } from '../pages/user/user';
import { UserPopover } from '../pages/user-popover/user-popover';
import { Users } from '../pages/users/users';
import { UserAchievements } from '../pages/user-achievements/user-achievements';
import { UserOverview } from '../pages/user-overview/user-overview';
import { UserGames } from '../pages/user-games/user-games';
import { UserFeed } from '../pages/user-feed/user-feed';
import { Login } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Game,
    Games,
    User,
    UserPopover,
    Users,
    UserAchievements,
    UserGames,
    UserOverview,
    UserFeed,
    Login
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Game,
    Games,
    User,
    UserPopover,
    Users,
    UserAchievements,
    UserGames,
    UserOverview,
    UserFeed,
    Login
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
        provide: ErrorHandler,
        useClass: IonicErrorHandler
    },
    GoogleAnalytics
  ]
})
export class AppModule {}
