import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { HomePage } from '../pages/home/home';
import { Games } from '../pages/games/games';
import { Users } from '../pages/users/users';
import { User } from '../pages/user/user';
import { Login } from '../pages/login/login';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = Users;

    pages: Array<{title: string, component: any}>;

    username: string = '';

    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public modalCtrl: ModalController,
        public storage: Storage,
        private ga: GoogleAnalytics
    ) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Games', component: Games },
            { title: 'Users', component: Users }
        ];

        this.storage.ready().then(() => {
            this.storage.get('username').then((val) => {
                this.username = val;
                if (this.username === null) {
                    this.showLogin();
                } else {
                    this.openUserPage(this.username);
                }
            });
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent("App Events", "App Started", "Open", 1);
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    showLogin() {
        let myModal = this.modalCtrl.create(Login);
        myModal.onDidDismiss(data => {
            this.username = data.username;
        });
        myModal.present();
    }

    getUserName() {
        return this.username;
    }

    openUserPage(username): void {
        this.nav.setRoot(User, {
            userID: username
        });
    }
}
