import { Component } from '@angular/core';
import { ViewController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

    username: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public storage: Storage,
    private ga: GoogleAnalytics
) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  saveUsername(): void {
      this.storage.ready().then(() => {

         this.storage.set('username', this.username);

         this.storage.get('username').then((val) => {
           console.log('Your name is', val);
         });

         this.closeModal();
       });

       this.ga.startTrackerWithId('UA-92641921-3')
       .then(() => {
           this.ga.trackView('Login');
       })
       .catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  closeModal(): void {
      this.viewCtrl.dismiss({
          'username': this.username
      });

      this.ga.startTrackerWithId('UA-92641921-3')
      .then(() => {
          console.log('Google analytics is ready now');
          this.ga.trackEvent("User", "Login", "Account", 1);
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
  }
}
