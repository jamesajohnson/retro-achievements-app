import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


import { RetroService } from '../../providers/retroservice';
import { UserPopover } from '../user-popover/user-popover';

@IonicPage()
@Component({
  selector: 'page-user-overview',
  templateUrl: 'user-overview.html',
})
export class UserOverview {

    userName: string;
    userSummary: Object;
    userFound: boolean = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        public popoverCtrl: PopoverController,
        private ga: GoogleAnalytics
    ) {

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserOverview');
  }

  ngOnInit(): void {
      this.userName = this.navParams.get('userID');

      this.retroService.getUserSummary(this.userName, 20).then(userSummary => {
          this.userSummary = userSummary;
          if (this.userSummary['MemberSince']) {
            this.userFound = true;
          }
          console.log(userSummary);
      });

      this.ga.startTrackerWithId('UA-92641921-3')
      .then(() => {
          this.ga.trackView('Game Overview');
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  presentPopover() {
      let popover = this.popoverCtrl.create(UserPopover);
      popover.present();
  }

}
