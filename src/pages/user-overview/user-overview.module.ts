import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserOverview } from './user-overview';

@NgModule({
  declarations: [
    UserOverview,
  ],
  imports: [
    IonicPageModule.forChild(UserOverview),
  ],
  exports: [
    UserOverview
  ]
})
export class UserOverviewModule {}
