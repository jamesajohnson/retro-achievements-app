import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPopover } from './user-popover';

@NgModule({
  declarations: [
    UserPopover,
  ],
  imports: [
    IonicPageModule.forChild(UserPopover),
  ],
  exports: [
    UserPopover
  ]
})
export class UserPopoverModule {}
