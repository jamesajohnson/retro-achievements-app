import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Login } from '../login/login';

@IonicPage()
@Component({
    selector: 'page-user-popover',
    templateUrl: 'user-popover.html',
})
export class UserPopover {

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage: Storage
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad UserPopover');
    }

    signout() {
        this.storage.ready().then(() => {
            this.storage.set('username', undefined);
            this.navCtrl.push(Login);
        });
    }
}
