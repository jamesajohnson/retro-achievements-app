import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { RetroService } from '../../providers/retroservice';

import { User } from '../user/user';

@IonicPage()
@Component({
    selector: 'page-users',
    templateUrl: 'users.html',
    providers: [
        RetroService
    ]
})
export class Users {

    topTenUsers: Object[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private retroService: RetroService,
        private ga: GoogleAnalytics
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad Users');
    }

    ngOnInit(): void {
        this.retroService.getTopTenUsers().then(topTenUsers => {
            this.topTenUsers = topTenUsers;
            console.log(topTenUsers);
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('Top Users');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    openUserPage(userID): void {
        this.navCtrl.push(User, {
            userID: userID
        });
    }

}
