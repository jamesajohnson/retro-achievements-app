import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { Games } from '../games/games';

import { RetroService } from '../../providers/retroservice';

@IonicPage()
@Component({
    selector: 'page-game',
    templateUrl: 'game.html',
    providers: [
        RetroService
    ]
})
export class Game {

    gameInfoExtended: Object;
    gameInfoAndUserProgress: Object;
    userProgress: Object;
    achievements: Object;
    gameID: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        public storage: Storage,
        private ga: GoogleAnalytics
    ) {
    }

    ngOnInit() {
        this.gameID = this.navParams.get('gameID');
        let userID = this.navParams.get('userID');

        if (!this.navParams.get('userID')) {
            this.storage.ready().then(() => {
                this.storage.get('username').then((val) => {
                    userID = val;
                });
            });
        } else {
            userID = this.navParams.get('userID')
        }

        this.retroService.getGameInfoExtended(this.gameID).then(gameInfoExtended => {
            this.gameInfoExtended = gameInfoExtended;

            this.ga.trackView('Game ' + gameInfoExtended['Title']);
        });

        this.retroService.getGameInfoAndUserProgress(userID, this.gameID).then(gameInfoAndUserProgress => {
            this.gameInfoAndUserProgress = gameInfoAndUserProgress;
            if (gameInfoAndUserProgress['Achievements']) {
                this.achievements = Object.keys(gameInfoAndUserProgress['Achievements']).map(function(e) {
                    return [Number(e), gameInfoAndUserProgress['Achievements'][e]];
                });
            }
            console.log(this.achievements);
        });

        this.retroService.getUserProgress(userID, this.gameID).then(userProgress => {
            this.userProgress = userProgress[this.gameID];
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('Game');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    userAchieved(ach): boolean {
        if ('DateEarned' in this.gameInfoAndUserProgress['Achievements'][ach.ID]) {
            return true;
        } else {
            return false;
        }
    }

    ionViewDidLoad() {

    }

    showGamesForConsole(consoleID) {
        this.navCtrl.push(Games, {
            consoleID: consoleID
        });
    }

    showBadgeModal(gameID) {
        console.log(gameID);
    }

}
