import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(
        public navCtrl: NavController,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('Home Page');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

    }

}
