import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserFeed } from './user-feed';

@NgModule({
  declarations: [
    UserFeed,
  ],
  imports: [
    IonicPageModule.forChild(UserFeed),
  ],
  exports: [
    UserFeed
  ]
})
export class UserFeedModule {}
