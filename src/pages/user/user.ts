import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { RetroService } from '../../providers/retroservice';

import { UserAchievements } from '../user-achievements/user-achievements';
import { UserGames } from '../user-games/user-games';
import { UserOverview } from '../user-overview/user-overview';
import { UserFeed } from '../user-feed/user-feed';

@IonicPage()
@Component({
    selector: 'page-user',
    templateUrl: 'user.html',
    providers: [
        RetroService
    ]
})
export class User {

    userAchievements: any;
    userGames: any;
    userFeed: any;
    userOverview: any;
    isUserProfile: boolean = false; //is this the page of the logged in user

    userName: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        public storage: Storage,
        private ga: GoogleAnalytics
    ) {
        this.userOverview = UserOverview;
        this.userAchievements = UserAchievements;
        this.userFeed = UserFeed;
        this.userGames = UserGames;
    }

    ngOnInit(): void {
        this.userName = this.navParams.get('userID');

        this.storage.ready().then(() => {
            this.storage.get('username').then((val) => {
                if (this.userName === val) {
                    this.isUserProfile = true;
                }
            });
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('User');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    ionViewDidLoad() {

    }

}
