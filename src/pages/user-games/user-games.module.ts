import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserGames } from './user-games';

@NgModule({
  declarations: [
    UserGames,
  ],
  imports: [
    IonicPageModule.forChild(UserGames),
  ],
  exports: [
    UserGames
  ]
})
export class UserGamesModule {}
