import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { RetroService } from '../../providers/retroservice';

import { Game } from '../game/game';

@IonicPage()
@Component({
    selector: 'page-user-games',
    templateUrl: 'user-games.html',
})
export class UserGames {

    userName: string;

    recentlyPlayed: Object[];
    allGames: Object[];
    awarded: Object;
    awardedArray: Object[] = [];
    achieved: Object;
    display: string = 'recent';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        private ga: GoogleAnalytics
    ) {

    }

    ngOnInit(): void {
        this.userName = this.navParams.get('userID');

        this.retroService.getUserSummary(this.userName, 20).then(userSummary => {
            this.recentlyPlayed = userSummary['RecentlyPlayed'];
            this.achieved = userSummary['Achieved'];
            this.awarded = userSummary['Awarded'];
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('User Games');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad UserGames');
    }

    viewGame(gameID) {
        this.navCtrl.push(Game, {
            gameID: gameID,
            userID: this.userName
        })
    }

    getGameAchievements(gameID) {
        return this.achieved[gameID];
    }

    getAwardedForGame(gameID) {
        let allAchForGame = this.awarded[gameID];
        return allAchForGame;
    }

}
