import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Games } from './games';

@NgModule({
  declarations: [
    Games,
  ],
  imports: [
    IonicPageModule.forChild(Games),
  ],
  exports: [
    Games
  ]
})
export class GamesModule {}
