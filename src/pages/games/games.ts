import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { RetroService } from '../../providers/retroservice';

import { Game } from '../game/game';

@IonicPage()
@Component({
    selector: 'page-games',
    templateUrl: 'games.html',
    providers: [
        RetroService
    ]
})
export class Games {

    defaultImage: string = '/Images/000001.png';

    consoles: Object[];
    games: Object[];
    filteredGames: Object[] = [];
    gamesCount: number = 0;
    consoleSelected = 4;

    searchControl: FormControl;
    searchTitle = '';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        private ga: GoogleAnalytics
    ) {
        this.searchControl = new FormControl();
    }

    ngOnInit(): void {

        this.retroService.getConsoleIDs().then(consoles => {
        console.log(consoles);
            this.consoles = consoles;
        });

        this.getGamesForConsole(this.consoleSelected);

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('All Games');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    ionViewDidLoad() {
        this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
            this.setFilteredItems();
        });
    }

    openGame(gameID): void {
        this.navCtrl.push(Game, {
            gameID: gameID
        });
    }

    getGamesForConsole(selectedConsole): void {
        this.retroService.getGameList(selectedConsole).then(games => {
            if (games) {
                this.games = games.filter(game => {
                    return game['ImageIcon'] !== this.defaultImage;
                });
                this.filteredGames = games;
                this.gamesCount = this.games.length;
                this.setFilteredItems();
            } else {
                this.games = [];
                this.filteredGames = this.games;
                this.gamesCount = 0;
            }
        });
    }

    setFilteredItems() {
        this.filteredGames = this.filterItems(this.searchTitle);
    }

    filterItems(searchTitle){
        console.log("filtering" + searchTitle);
        if (this.games) {
            return this.games.filter((item) => {
                return item['Title'].toLowerCase().indexOf(searchTitle.toLowerCase()) > -1;
            });
        } else {
            return [];
        }
    }

}
