import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserAchievements } from './user-achievements';

@NgModule({
  declarations: [
    UserAchievements,
  ],
  imports: [
    IonicPageModule.forChild(UserAchievements),
  ],
  exports: [
    UserAchievements
  ]
})
export class UserAchievementsModule {}
