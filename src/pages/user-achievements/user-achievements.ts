import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { RetroService } from '../../providers/retroservice';

import { Game } from '../game/game';

@IonicPage()
@Component({
    selector: 'page-user-achievements',
    templateUrl: 'user-achievements.html',
})
export class UserAchievements {

    userName: string;

    awarded: Object[];
    awardedArray: Object[] = [];

    firstPageLoaded: boolean = false;
    feedOffset = 0;
    feedPageLength = 100;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public retroService: RetroService,
        private ga: GoogleAnalytics
    ) {

    }

    ngOnInit(): void {
        this.userName = this.navParams.get('userID');

        this.retroService.getUserSummary(this.userName, 20).then(userSummary => {
            this.awarded = Object.keys(userSummary['Awarded']).map(function(e) {
                return [Number(e), userSummary['Awarded'][e]];
            });
            console.log(userSummary);
        });

        // get first page of feed
        this.retroService.getFeedFor(this.userName, this.feedPageLength, this.feedOffset).then(feed => {
            for(let f of feed) {
                if (f['GameIcon'] !== null) {
                    this.awardedArray.push(f);
                }
            }
            this.firstPageLoaded = true;
            this.feedOffset += this.feedPageLength;
        });

        this.ga.startTrackerWithId('UA-92641921-3')
        .then(() => {
            this.ga.trackView('User Achievements');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
    }

    // TODO, are these values correct, is anything skipped, what happend if I scroll right to the bottom?
    getPageFeed(infiniteScroll) {
        if (this.firstPageLoaded) {
            this.retroService.getFeedFor(this.userName, this.feedPageLength, this.feedOffset).then(feed => {
                for(let f of feed) {
                    if (f['GameIcon'] !== null)
                        this.awardedArray.push(f);
                }
                this.feedOffset += this.feedPageLength;
                infiniteScroll.complete();
            });
        }
    }

    buildAwardedArray(awarded) {
        for (let game in awarded) {
            this.retroService.getGameInfo(game).then(gameInfo => {
                console.log(gameInfo);
                this.awardedArray.push({
                    gameID: game,
                    gameInfo: gameInfo,
                    awarded: awarded[game]
                });
                console.log(this.awardedArray);
            });
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad UserAchievements');
    }

    viewGame(gameID) {
        this.navCtrl.push(Game, {
            gameID: gameID,
            userID: this.userName
        })
    }

}
