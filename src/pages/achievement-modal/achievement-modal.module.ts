import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AchievementModalPage } from './achievement-modal';

@NgModule({
  declarations: [
    AchievementModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AchievementModalPage),
  ],
  exports: [
    AchievementModalPage
  ]
})
export class AchievementModalPageModule {}
