import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RetroService {

    API_URL: string = 'http://retroachievements.org/API/';
    ra_user: string = 'howdoyoudoken';
    ra_api_key: string = 'n07CK3UIxUaJ8yS1MHv6C5jWA9L7rIXF';

    constructor(public http: Http) {
        console.log('Hello Retroservice Provider');
    }

    AuthQS = function() {
        return "?z=" + this.ra_user + "&y=" + this.ra_api_key;
    }

    GetRAURL = function(target, params = "") {
        return this.API_URL + target + this.AuthQS() + `&${params}`;
    }

    getTopTenUsers(): Promise<Object[]> {
        return this.http.get(this.GetRAURL('API_GetTopTenUsers.php'))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getGameInfo(gameID): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetGame.php", `i=${gameID}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getGameInfoExtended(gameID): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetGameExtended.php", `i=${gameID}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getConsoleIDs(): Promise<Object[]> {
        return this.http.get(this.GetRAURL( 'API_GetConsoleIDs.php' ))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getGameList(consoleID): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetGameList.php", `i=${consoleID}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getFeedFor(user, count, offset = 0): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetFeed.php", `u=${user}&c=${count}&o=${offset}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getUserRankAndScore(user): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetUserRankAndScore.php", `u=${user}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getUserProgress(user, gameIDCSV): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetUserProgress.php", `u=${user}&i=${gameIDCSV}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getUserRecentlyPlayedGames(user, count, offset = 0): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetUserRecentlyPlayedGames.php", `u=${user}&c=${count}&o=${offset}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getUserSummary(user, numRecentGames): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetUserSummary.php", `u=${user}&g=${numRecentGames}&a=5`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getGameInfoAndUserProgress(user, gameID): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetGameInfoAndUserProgress.php", `u=${user}&g=${gameID}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    getAchievementsEarnedOnDay(user, dateInput): Promise<Object[]> {
        return this.http.get(this.GetRAURL("API_GetAchievementsEarnedOnDay.php", `u=${user}&d=${dateInput}`))
        .toPromise()
        .then(response => response.json() as Object[])
        .catch(this.handleError);
    }

    handleError = function() {
        console.log('Error in retro service');
    }
    /*
    GetAchievementsEarnedBetween = function(user, dateStart, dateEnd) {
        dateFrom = strtotime( dateStart );
        dateTo = strtotime( dateEnd );
        return this.http.get(this.GetRAURL( "API_GetAchievementsEarnedBetween.php", `u=${user}&f=${dateFrom}&t=${dateTo}` ));
    }
    */
}
